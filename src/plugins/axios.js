import axios from 'axios'

const config = {
  baseURL: process.env.VUE_APP_PADAWAN_API_URL
}

const _axios = axios.create(config)

export default _axios
