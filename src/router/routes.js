const Sales = () => import(/* webpackChunkName: "Sales" */ '../views/Sales.vue')
const Lots = () => import(/* webpackChunkName: "Lots" */ '../views/Lots.vue')
const Search = () => import(/* webpackChunkName: "Search" */ '../views/Search.vue')
const Page404 = () => import(/* webpackChunkName: "Page404" */ '../views/Page404.vue')

const routes = [
  {
    path: '*',
    redirect: '/404'
  },
  {
    path: '/',
    redirect: '/sales'
  },
  {
    path: '/sales',
    name: 'sales',
    component: Sales
  },
  {
    path: '/lots/:saleId',
    name: 'lots',
    component: Lots
  },
  {
    path: '/search',
    name: 'search',
    component: Search
  },
  {
    path: '/404',
    name: '404',
    component: Page404
  }
]

export default routes
