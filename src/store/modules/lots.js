import axios from '@/plugins/axios'

const state = {
  lots: []
}
const mutations = {
  SET_LOTS (state, data) {
    state.lots = data
  }
}

const actions = {
  createLots (context, data) {
    const promises = data.lots.map(lot => {
      return axios.post('/items', {
        sale_id: data.saleId,
        description: lot.description
      })
    })
    return Promise.all(promises)
  },
  getLots (context, data) {
    return axios.get('/items', {
      params: {
        sale_id: data
      }
    })
  }
}

const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
