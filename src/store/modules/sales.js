import axios from '@/plugins/axios'

const state = {
  sales: []
}
const mutations = {
  SET_SALES (state, data) {
    state.sales = data
  }
}

const actions = {
  createSale (context, data) {
    return axios.post('/sales', {
      title: data.title,
      description: data.description
    })
  },
  getSales () {
    return axios.get('/sales')
  }
}

const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
