import Vue from 'vue'
import Vuex from 'vuex'
import axios from '@/plugins/axios'

import sales from './modules/sales'
import lots from './modules/lots'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    sales,
    lots
  },
  state: {
    snackbar: {},
    resultsSearch: []
  },
  mutations: {
    SET_SNACKBAR (state, data) {
      state.snackbar = data
    },
    SET_RESULTS_SEARCH (state, data) {
      state.resultsSearch = data
    }
  },
  actions: {
    async search (context, data) {
      try {
        const params = {
          params: { q: data}
        }
        const promises = [
          axios.get('/sales', params),
          axios.get('/items', params)
        ]
        const result = await Promise.all(promises)
        const sales = result[0].data.map(sale => {
          return {
            type: 'sale',
            ...sale
          }
        })
        const lots = result[1].data.map(lot => {
          return {
            type: 'lot',
            ...lot
          }
        })
        context.commit('SET_RESULTS_SEARCH', [ ...sales, ...lots])
      } catch (error) {
        console.error('Error occured when searchin sale & lot : ', error)
      }
    }
  }
})
